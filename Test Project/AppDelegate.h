//
//  AppDelegate.h
//  Test Project
//
//  Created by Krystian Śliwa on 17.04.2015.
//  Copyright (c) 2015 Droids On Roids. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

