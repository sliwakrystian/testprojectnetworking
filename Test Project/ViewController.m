//
//  ViewController.m
//  Test Project
//
//  Created by Krystian Śliwa on 17.04.2015.
//  Copyright (c) 2015 Droids On Roids. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () //<>
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSMutableData *imageData;

@end

@implementation ViewController

- (IBAction)startAction:(id)sender {
    
    [self downloadContent];
}

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)downloadContent {
    
//    self.imageData = [NSMutableData data];
//
//    NSURL *imageUrl = [NSURL URLWithString:@"http://lorempixel.com/500/500/sports/1"];
//    NSURLRequest *request = [NSURLRequest requestWithURL:imageUrl];

//    NSURLSession *session = [NSURLSession sessionWithConfiguration:<#(NSURLSessionConfiguration *)#>]
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.droidsonroids.pl"]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
